<?php

namespace App\Repository;

use App\Entity\ValidatedCart;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ValidatedCart|null find($id, $lockMode = null, $lockVersion = null)
 * @method ValidatedCart|null findOneBy(array $criteria, array $orderBy = null)
 * @method ValidatedCart[]    findAll()
 * @method ValidatedCart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValidatedCartRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ValidatedCart::class);
    }

    // /**
    //  * @return ValidatedCart[] Returns an array of ValidatedCart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ValidatedCart
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
