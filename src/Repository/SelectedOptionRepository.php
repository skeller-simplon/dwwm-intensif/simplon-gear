<?php

namespace App\Repository;

use App\Entity\SelectedOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SelectedOption|null find($id, $lockMode = null, $lockVersion = null)
 * @method SelectedOption|null findOneBy(array $criteria, array $orderBy = null)
 * @method SelectedOption[]    findAll()
 * @method SelectedOption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SelectedOptionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SelectedOption::class);
    }

    // /**
    //  * @return SelectedOption[] Returns an array of SelectedOption objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SelectedOption
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
