<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Owner;
use App\Entity\Category;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findByName($word)
    {
        return $this->createQueryBuilder('article')
            ->andWhere('article.name LIKE :word')
            ->setParameter(':word', '%' . $word . '%')
            ->getQuery()
            ->getResult();
    }
    public function findByOwner(Owner $owner)
    {
        return $this->createQueryBuilder('owner_article')
            ->andWhere('owner_id LIKE :id')
            ->setParameter(':id',  $owner->getId())
            ->getQuery()
            ->getResult();
    }

    public function findByOwnerCategories(int $owner = null, int $category = null, string $sort = NULL){
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.owners', 'o')
            ->orderBy('a.price', $sort);
        if($owner) {
            $query->andWhere('o.id = :owner')
            ->setParameter('owner', $owner);
        }
        if($category) {
            $query->andWhere('a.category = :category')
            ->setParameter('category', $category);
        }
        return $query->getQuery()->getResult();
    }
}