<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Service\UploadService;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ArticleRepository $articleRepository)
    {
        return $this->render('admin/admin.html.twig', [
            "articles" => $articleRepository->findAll()
        ]);
    }
     /**
     * @Route("/admin/add-article", name="add_article")
     */
    public function addArticle(Request $request, ObjectManager $manager, UploadService $service)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($article->getfilePhoto()) {
                $article->setPhoto($service->upload($article->getfilePhoto()));
            }
            $manager->persist($article);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->render("article/add-article.html.twig", [
            "form" => $form->createView()
        ]);
    }
     /**
     * @Route("/admin/remove/article/{article}", name="remove")
     */
    public function remove(Article $article, ObjectManager $manager){
        $manager->remove($article);
        $manager->flush();
        return $this->redirectToRoute('admin');
    }
    /**
     * @Route("/admin/modify/{article}", name="modify")
     */
    public function modify(Request $request, Article $article, ObjectManager $manager)
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) { 
            $manager->persist($article);
            $manager->flush();

            // return $this->redirectToRoute('admin');
        }
        return $this->render("article/add-article.html.twig", [
            "form" => $form->createView()
        ]);
    }
}