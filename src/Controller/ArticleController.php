<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use App\Entity\Article;
use App\Entity\Comment;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\CommentType;
use App\Entity\Owner;
use App\Repository\OwnerRepository;
use App\Repository\CategoryRepository;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article/article={article}", name="article")
     */
    public function article(Article $article, Request $request, ObjectManager $manager)
    {
        $comment = new Comment;
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setArticle($article);
            $comment->setDate(new \DateTime());
            $comment->setUser($this->getUser());
            $manager->persist($comment);
            $manager->flush();
        }
        return $this->render("article/article.html.twig", [
            "article" => $article,
            "form" => $form->createView(),  
            "owner" => $article->getOwners()->last()  
        ]);
    }

    /**
     * @Route("/articles", name="all-articles")
     */
    public function allArticles(ArticleRepository $articleRepository, OwnerRepository $ownerRepository, CategoryRepository $categoryRepository, Request $request)
    {
        if ($request->get("owner") != null) {
            return $this->render("article/all-articles.html.twig", [
                "articles" => $articleRepository->findByOwnerCategories($request->get("owner"), 
                                                                        $request->get("category"), 
                                                                        $request->get("sort")
                                                                    ),
                "title" => $ownerRepository->find(intval($request->get("owner")))
            ]);
        } elseif ($request->get("category") != null) {
            dump(intval($request->get("category")));
            return $this->render("article/all-articles.html.twig", [
                "articles" => $articleRepository->findByOwnerCategories($request->get("owner"), 
                                                                        $request->get("category"), 
                                                                        $request->get("sort")
                                                                    ),
                "title" => $categoryRepository->find(intval($request->get("category")))
            ]);
        }
        return $this->render(("article/all-articles.html.twig"), [
            "articles" => $articleRepository->findByOwnerCategories($request->get("owner"), $request->get("category"), $request->get("sort"))
        ]);
    }

    /**
     * @Route ("/search-article", name="search_article")
     */
    public function searchArticle(Request $request, ArticleRepository $repo)
    {
        dump($request->get('search'));
        $results = $repo->findByName($request->get('search'));

        return $this->render('article/search-article.html.twig', [
            'results' => $results
        ]);
    }
}
