<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartArticleRepository")
 */
class CartArticle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Article")
     * @ORM\JoinColumn(nullable=true)
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="wishArticles")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="cartArticles")
     * @ORM\JoinColumn(nullable=true)
     */
    private $userCart;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ValidatedCart", inversedBy="cartArticles")
     */
    private $linkedValidatedCart;


    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThan(0)
     */
    private $quantity;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SelectedOption", mappedBy="cartArticle")
     */
    private $selectedOptions;

    public function __construct()
    {
        $this->selectedOptions = new ArrayCollection();
    }

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
    public function getUserCart(): ?User
    {
        return $this->userCart;
    }

    public function setUserCart(?User $user): self
    {
        $this->userCart = $user;

        return $this;
    }

    public function getLinkedValidatedCart(): ?ValidatedCart
    {
        return $this->linkedValidatedCart;
    }

    public function setLinkedValidatedCart(?ValidatedCart $linkedValidatedCart): self
    {
        $this->linkedValidatedCart = $linkedValidatedCart;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return Collection|SelectedOption[]
     */
    public function getSelectedOptions(): collection
    {
        return $this->selectedOptions;
    }

    public function addSelectedOption(SelectedOption $selectedOption): self
    {
        if (!$this->selectedOptions->contains($selectedOption)) {
            $this->selectedOptions[] = $selectedOption;
            $selectedOption->setCartArticle($this);
        }

        return $this;
    }

    public function removeSelectedOption(SelectedOption $selectedOption): self
    {
        if ($this->selectedOptions->contains($selectedOption)) {
            $this->selectedOptions->removeElement($selectedOption);
            // set the owning side to null (unless already changed)
            if ($selectedOption->getCartArticle() === $this) {
                $selectedOption->setCartArticle(null);
            }
        }

        return $this;
    }
}
