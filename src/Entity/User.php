<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *     message = "Votre email n'est pas un email valide.")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull
     * @Assert\Length(
     *      min = 5,
     *      minMessage = "Votre mot de passe doit contenir au moins 5 caractères."
     * )
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotNull
     * @Assert\Regex (
     *      pattern="^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}^"
     * )
     * 
     */
    private $phoneNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="user")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartArticle", mappedBy="userCart")
     */
    private $cartArticles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartArticle", mappedBy="user")
     */
    private $wishArticles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ValidatedCart", mappedBy="user")
     */
    private $ValidatedCarts;

    /**
     * @ORM\Column(type="array")
     */
    private $roles = [];

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->wishList = new ArrayCollection();
        $this->cart = new ArrayCollection();
        $this->cartArticles = new ArrayCollection();
        $this->wishArticles = new ArrayCollection();
        $this->ValidatedCarts = new ArrayCollection();
    }

    public function getId(): ? int
    {
        return $this->id;
    }

    public function getEmail(): ? string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ? string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ? string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ? string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getAddress(): ? string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhoneNumber(): ? string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }
        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CartArticle[]
     */
    public function getCartArticles(): Collection
    {
        return $this->cartArticles;
    }

    public function addCartArticle(CartArticle $cartArticle): self
    {
        if (!$this->cartArticles->contains($cartArticle)) {
            $this->cartArticles[] = $cartArticle;
            $cartArticle->setUserCart($this);
        }

        return $this;
    }

    public function removeCartArticle(CartArticle $cartArticle): self
    {
        if ($this->cartArticles->contains($cartArticle)) {
            $this->cartArticles->removeElement($cartArticle);
            // set the owning side to null (unless already changed)
            if ($cartArticle->getUserCart() === $this) {
                $cartArticle->setUserCart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CartArticle[]
     */
    public function getWishArticles(): Collection
    {
        return $this->wishArticles;
    }

    public function addWishArticle(CartArticle $wishArticle): self
    {
        if (!$this->wishArticles->contains($wishArticle)) {
            $this->wishArticles[] = $wishArticle;
            $wishArticle->setUser($this);
        }

        return $this;
    }

    public function removeWishArticle(CartArticle $wishArticle): self
    {
        if ($this->wishArticles->contains($wishArticle)) {
            $this->wishArticles->removeElement($wishArticle);
            // set the owning side to null (unless already changed)
            if ($wishArticle->getUser() === $this) {
                $wishArticle->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ValidatedCart[]
     */
    public function getValidatedCarts(): Collection
    {
        return $this->ValidatedCarts;
    }

    public function addValidatedCart(ValidatedCart $ValidatedCart): self
    {
        if (!$this->ValidatedCarts->contains($ValidatedCart)) {
            $this->ValidatedCarts[] = $ValidatedCart;
            $ValidatedCart->setUser($this);
        }

        return $this;
    }

    public function removeValidatedCart(ValidatedCart $ValidatedCart): self
    {
        if ($this->ValidatedCarts->contains($ValidatedCart)) {
            $this->ValidatedCarts->removeElement($ValidatedCart);
            // set the owning side to null (unless already changed)
            if ($ValidatedCart->getUser() === $this) {
                $ValidatedCart->setUser(null);
            }
        }
        return $this;
    }

    /**
     * @return (Role|string)[] The user roles
     */
    public function getRoles(): ? array
    {
        return $this->roles;
    }
    public function addRole(string $role): self
    {
        $this->roles[] = $role;
        return $this;
    }
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    /**
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }
    /**
     * @return string|null The salt
     */
    public function getSalt()
    { }
    public function eraseCredentials()
    { }
    public function __toString()
    {
        return $this->name . ' ' . $this->surname;
    }
}
