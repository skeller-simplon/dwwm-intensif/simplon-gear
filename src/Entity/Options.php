<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionsRepository")
 */
class Options
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $choice = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Article", inversedBy="options")
     * @ORM\JoinColumn(nullable=false)
     */
    private $article;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SelectedOption", mappedBy="originOption")
     */
    private $selectedOptions;
    

    public function __construct()
    {
        $this->selectedOptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getChoice(): ?array
    {
        return $this->choice;
    }

    public function setChoice(?array $choice): self
    {
        $this->choice = $choice;

        return $this;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return Collection|SelectedOption[]
     */
    public function getSelectedOptions(): Collection
    {
        return $this->selectedOptions;
    }

    public function addSelectedOption(SelectedOption $selectedOption): self
    {
        if (!$this->selectedOptions->contains($selectedOption)) {
            $this->selectedOptions[] = $selectedOption;
            $selectedOption->setOriginOption($this);
        }

        return $this;
    }

    public function removeSelectedOption(SelectedOption $selectedOption): self
    {
        if ($this->selectedOptions->contains($selectedOption)) {
            $this->selectedOptions->removeElement($selectedOption);
            // set the owning side to null (unless already changed)
            if ($selectedOption->getOriginOption() === $this) {
                $selectedOption->setOriginOption(null);
            }
        }
        return $this;
    }
    public function __toString()
    {
        return $this->label;
    }
}
