# Simplon GEAR

## Présentation du projet

Le but de ce projet est d'utiliser les competences aquises avec **SYMFONY 4** durant la formation. nous avons également utilisé **DOCTRINE** pour ce projet. 
Dans cet esprit nous avons eu un mois pour réaliser un site e-commerce (SimplonGear).



Le but de ce site est de proposer à la vente des objets appartenant à nos formateurs (comme [The Celebrity Dresses](thecelebritydresses.com)) ainsi que des produits dérivés **Simplon** à leur éffigie.

L'intérêt est d'apprendre les subtilitées d'un site e-commerce de manière ludique.



## Languages utilisées

* **HTML5 / CSS 3**
  * Boostrap 4
  * [Bootswatch](https://bootswatch.com/)

* **MYSQL**

* **PHP 7**

  * SYMFONY 4
  * DOCTRINE
  * [Faker](https://github.com/fzaninotto/Faker)

* **TWIG**

## Interet

Utilisation et apprentissage de **SYMFONY 4**, **DOCTRINE**, **PHP 7**, **MYSQL** et le travail en équipe via git.

Apprentissage et réalisation de tests fonctionnels pour l'application.

Le projet a fonctionné avec la méthode agile: quatre sprints pour les quatre semaines et un scrum master tournant pour chaque sprint. Les objectifs de sprints et leur taches assignées peuvent être retrouvées dans la partie board du projet.



## Installation/ Utilisation

1. **Cloner le projet**
2. Installer les dépendances avec **composer install**
3. Créer un fichier .env.local avec dedans DATABASE_URL=mysql://user:password@127.0.0.1:3306/db_name en remplaçant user, password et db_name par vos informations de connexion à votre base de données
4. Faire un bin/console **doctrine:migrations:migrate** pour mettre la bdd dans le bon état (si jamais cela ne marche pas, tenter de faire un bin/console doctrine:schema:drop --force avant de refaire le migrate pour remettre à zéro la bdd)

Le projet dispose de fixtures chargeable avec doctrine : doctrine:fixtures:load



## Fonctionnalitées intéressantes

* Upload d'image dans le projet
* Gestion de la sécurité des différentes pages
* Système de tri par prix en fonction des catégories et owners



## User Stories

#### Contraintes obligatoires : 

1. En tant qu'utilisateur.ice, je veux pouvoir visualiser la liste des produits filtrés pour faciliter mon choix
2. En tant qu'utilisateur.ice, je veux pouvoir ajouter des produits à mon panier pour passer rapidement ma commande
3. En tant que visiteur.euse, je veux pouvoir créer mon compte pour accéder à des fonctionnalités supplémentaires du site
4. En tant qu'administrateur.ice, je veux pouvoir gérer les produits disponibles afin de modifier le catalogue



#### Contraintes supplémentaires:

1. En tant que visiteur.euse, je veux pouvoir rechercher des articles avec une barre de recherche
2. En tant que visiteur.euse, je veux pouvoir voir quels sont les articles en rapport avec l'article que je regarde
3. En tant qu'utilisateur.ice, je veux pouvoir noter et commenter les produits
4. En tant qu'utilisateur.ici, je veux pouvoir ajouter des articles à une liste de souhaits pour les ajouter à mon profil plus tard

## Diagrammes  

![20% center](./doc/UseCaseSimplonGear.png)
![20% center](./doc/diagramm-class.png)

## Wireframe et design actuel

![20% center](./doc/WireframeSimplonGear.png)


![20% center](./doc/acceuilSimplonGear.png)

...



## Exemple de code utilisé

Le code ci-dessous retrouvable dans /src/Controller/ArticleController vise à faire que dans un ajout d'article au panier, si l'article y existe déjà avec les mêmes caractéristiques et s'ajoute en quantité au précédent plutôt que d'y être en double.

```php
$tempOptions = [];
        foreach ($article->getOptions() as $key => $option) {
            $selectedOption = new SelectedOption();
            $selectedOption->setLabel($option->getLabel());
            $selectedOption->setOriginOption($option);
            $selectedOption->setChoice($request->get($option->getLabel()));
            $tempOptions[] = $selectedOption;
        }

        $matchCartArticle = $cartArticleRepository->findOneBy(['article' => $article, 'userCart' => $this->getUser()]);
        if ($matchCartArticle != null) {
            if ($matchCartArticle->getSelectedOptions()->toArray() == $tempOptions) {
                $matchCartArticle->setQuantity($matchCartArticle->getQuantity() + intval($request->get('quantity')));
                $manager->persist($matchCartArticle);
                if ($request->get('cart') == "cart") {
                    $matchCartArticle->setUserCart($this->getUser());
                } elseif ($request->get('wish') == "wish") {
                    $matchCartArticle->setUser($this->getUser());
                }
                $manager->flush();
                return $this->redirectToRoute('article', [
                    'article' => $article->getId()
                ]);
            }
        }
```



## Résultat du projet

#### Difficultées :

Melissa: Quelques algorithmes et les relations entre entitées

Samuel: Complexité du projet et frontend

David: Problème d'application du PHP vu en cours 

#### Ce qui a été plaisant :

Melissa: BackEnd & tests fonctionnels

Samuel: L'algorithmie en équipe & processus agile

David: J'ai aimer faire le front avec **BOOTSTRAP**, **BOOTSWATCH** et **TWIG**



## License

Le projet et son code peuvent être utilisés sans limitations.

Toutes les photos sont la propriété de leurs propriétaires respectifs.

(pour voir mes autre création rendez-vous sur [Gitlab](https://gitlab.com/dashboard/projects).)

